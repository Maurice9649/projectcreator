import argparse
import sys
import json

def configureTemplate(projectJson):
    print("Configuring Template")
    print("JSON:")
    print(json.dumps(projectJson))


def main() -> int:
    parser = argparse.ArgumentParser()

    required = parser.add_argument_group('required arguments')
    required.add_argument("--step",
        help = "What step of the generation process is being executed",
        required=True
    )
    required.add_argument("--project",
        help = "Path to the projects json file",
        required=True
    )

    args = parser.parse_args()

    with open(args.project,"rt") as projectFile:
        projectJson = json.load(projectFile)
        if args.step == "configureTemplate":
            configureTemplate(projectJson)
        else:
            print(f"Unknown/Unsupported step '{args.step}', ignoring")
    return 0

if __name__ == '__main__':
    sys.exit(main())