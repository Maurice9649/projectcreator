#pragma once
#include <qexception.h>
#include <string>
/// Helper Macro declaring a basic QException, @see https://doc.qt.io/qt-6/qexception.html
#define CUSTOM_QException(name) \
    class name : public QException \
    { \
        std::string message {}; \
    public: \
        name() = default; \
        name(const char* msg) : message(msg) {} \
        name(std::string msg) : message(std::move(msg)) {} \
        name(const QString& msg) : message(msg.toStdString()) {} \
        void raise() const override { throw* this; } \
        name* clone() const override { return new name(*this); } \
        virtual inline const char* what() const noexcept { return message.c_str(); };\
    };

CUSTOM_QException(ParseException)
CUSTOM_QException(NotImplementedException)