#include "project.h"

#include <qjsonobject.h>
#include <qjsonarray.h>
#include <qmetaobject.h>

#include "exceptions.h"

QJsonObject Project::toJson() const
{
	return 
	{
		{"options",QJsonObject::fromVariantMap(OptionValues)}
	};
}
