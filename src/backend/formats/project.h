#pragma once
#include <optional>
#include <QJsonDocument>
#include "option.h"

/// <summary>
/// File containg all info about the project, passed to the configure script
/// </summary>
struct Project
{
	Q_GADGET
public:
	
	QVariantMap OptionValues {};

	QJsonObject toJson() const;
};
