#pragma once
#include <optional>
#include <QJsonDocument>

struct Option
{
	Q_GADGET
public:
	/// <summary>
	/// Name displayed in the UI
	/// </summary>
	QString Name				{ "UNDEFINED" };
	/// <summary>
	/// Help text displayed next to the name
	/// </summary>
	QString Help				{};
	/// <summary>
	/// Shown when the data provided by the user is invalid.
	/// </summary>
	QString FormatHint			{};

	static QVariant fromJson(const QJsonObject&);
};

struct OptionString : public Option
{
	Q_GADGET
public:
	static constexpr QLatin1String TypeName { "String" };
	static OptionString fromJson(const QJsonObject&, Option);

};
struct OptionInt : public Option
{
	Q_GADGET
public:
	static constexpr QLatin1String TypeName{ "Int" };

	static OptionInt fromJson(const QJsonObject&, Option);
};
