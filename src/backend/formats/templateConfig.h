#pragma once
#include <optional>
#include <QJsonDocument>
#include "option.h"

struct TemplateConfig
{
	Q_GADGET

	/// <summary>
	/// A Unique ID for this TemplateConfig.
	/// Populated by the Provider
	/// </summary>
	QString UniqueID{};
public:

	inline void InitUniqueID(QString id)
	{
		Q_ASSERT(UniqueID.isEmpty());
		UniqueID = std::move(id);
	}
	inline const QString& GetUniqueID() const
	{
		return UniqueID;
	}

	enum class ProjectType
	{
		Generic,
		Unreal,
	};
	Q_ENUM(ProjectType)

	/// <summary>
	/// Name displayed in the UI
	/// </summary>
	QString Name				{ "UNNAMMED" };
	/// <summary>
	/// List of tags used to filter for specific modules
	/// </summary>
	QStringList Tags			{};
	/// <summary>
	/// Type for special functionallity
	/// </summary>
	ProjectType	Type			{ ProjectType::Generic };
	/// <summary>
	/// List of options, that will be passed to the configure command
	/// </summary>
	QList<QVariant> Options		{};
	/// <summary>
	/// Command to run to configure the template with given options
	/// </summary>
	QString ConfigureCommand	{};



	static TemplateConfig fromJson(const QJsonObject&);
};
