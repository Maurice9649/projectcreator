#include "option.h"

#include <qjsonobject.h>
#include <array>

#include "exceptions.h"

namespace
{
	using constructFunc = QVariant (*) (const QJsonObject&, Option base);
	using typeMapValue = QPair<const QLatin1String, constructFunc>;

	/// <summary>
	/// Calls T::fromJson and casts to a Variant
	/// </summary>
	template<typename T>
	constexpr QVariant fromJsonVariant(const QJsonObject& obj, Option base)
	{
		return QVariant::fromValue(T::fromJson(obj,base));
	}
	/// <summary>
	/// Generates a Pair out of the type name, and a parsing function
	/// </summary>
	template<typename T>
	constexpr typeMapValue makeTypeMapPair()
	{
		return { T::TypeName , fromJsonVariant<T> };
	}

	constexpr std::array<typeMapValue, 2> typeNameMap
	{
		makeTypeMapPair<OptionString>(),
		makeTypeMapPair<OptionInt>(),
	};
}


QVariant Option::fromJson(const QJsonObject& obj)
{
	auto type = obj["type"];
	if (!type.isString())
	{
		throw ParseException("'type' is not a string");
	}

	// Get correct fromJson function
	auto typeNameMatches = [typeString = type.toString()](typeMapValue typeName) { return typeName.first == typeString; };
	auto typeMapItr = std::find_if(typeNameMap.begin(), typeNameMap.end(), typeNameMatches);
	if (typeMapItr == typeNameMap.end())
	{
		throw ParseException(type.toString() + " unknown option type");
	}
	auto fromJson = (*typeMapItr).second;

	// Parse Base Data
	Option baseOption {};

	auto name = obj["name"];
	if (!name.isString())
	{
		throw ParseException("'name' is not a string");
	}
	baseOption.Name = name.toString();

	if (obj.contains("help"))
	{
		auto help = obj["help"];
		if (!help.isString())
		{
			throw ParseException("'help' is not a string");
		}
		baseOption.Help = help.toString();
	}

	if (obj.contains("formatHint"))
	{
		auto formatHint = obj["formatHint"];
		if (!formatHint.isString())
		{
			throw ParseException("'formatHint' is not a string");
		}
		baseOption.FormatHint = formatHint.toString();
	}

	return fromJson(obj, std::move(baseOption));
}

OptionString OptionString::fromJson(const QJsonObject& obj, Option base)
{
	OptionString option{ std::move(base) };

	return option;
}

OptionInt OptionInt::fromJson(const QJsonObject& obj, Option base)
{
	OptionInt option{ std::move(base) };

	return option;
}
