#include "templateConfig.h"

#include <qjsonobject.h>
#include <qjsonarray.h>
#include <qmetaobject.h>

#include "exceptions.h"

TemplateConfig TemplateConfig::fromJson(const QJsonObject& obj)
{
	TemplateConfig config{};
	// Name
	auto name = obj["name"];
	if (!name.isString())
	{
		throw ParseException("'name' was not a string!");
	}
	config.Name = name.toString();

	// Tags
	auto tags = obj["tags"];
	if (!tags.isArray())
	{
		throw ParseException("'tags' was not an array!");
	}
	QJsonArray tagArray = tags.toArray();

	config.Tags.reserve(tagArray.size());
	for (size_t i = 0; i < tagArray.size(); i++)
	{
		auto elem = tagArray[i];
		if (!elem.isString())
		{
			throw ParseException("Element #" + QString::number(i) + "in 'tags' was not a string!");
		}
		config.Tags.append(elem.toString());
	}

	// Type
	auto typeEnum = QMetaEnum::fromType<ProjectType>();
	auto type = obj["type"];
	if (!type.isString())
	{
		throw ParseException("'type' was not a string!");
	}
	bool ok;
	std::string typeStdString = type.toString().toStdString();
	const char* typeCharPtr = typeStdString.c_str();
	config.Type = (ProjectType)typeEnum.keyToValue(typeCharPtr, &ok);
	if (!ok)
	{
		throw ParseException(type.toString() + "was not a valid value for type!");
	}

	// Options
	auto options = obj["options"];
	if (!options.isArray())
	{
		throw ParseException("'options' was not an array!");
	}
	QJsonArray optionsArray = options.toArray();

	config.Options.reserve(optionsArray.size());
	for (size_t i = 0; i < optionsArray.size(); i++)
	{
		auto elem = optionsArray[i];
		if (!elem.isObject())
		{
			throw ParseException(QString("Element #") + QString::number(i) + "in 'options' was not a object!");
		}
		auto newOption = Option::fromJson(elem.toObject());
		config.Options.append(newOption);
	}

	// configure Command
	auto configureCommand = obj["configureCommand"];
	if (name.isString())
	{
		config.ConfigureCommand = configureCommand.toString();
	}
	else if (!name.isUndefined())
	{
		throw ParseException("'configureCommand' was neither a string nor empty!");
	}

	return config;
}
