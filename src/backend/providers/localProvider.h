#pragma
#include "iprovider.h"
#include "exceptions.h"
#include <optional>

class LocalProvider : public QObject, public IProvider
{
	Q_OBJECT

	QDir Path;

	void copyDirRecursive(QString inputPath, QString outputPath) const;
public:

	LocalProvider(QDir path, QObject* parent = nullptr);

	virtual QString GetProviderName() const { return QString("Local Directory (%1)").arg(Path.dirName()); };

	[[nodiscard]]
	virtual QList<TemplateConfig> GetTemplates(Feedback& feedback) const;
	
	virtual void CopyTemplateFiles(TemplateConfig cfg, QDir outputPath) const;
};