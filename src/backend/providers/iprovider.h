#pragma once

#include <QObject>
#include <QDir>
#include "formats/templateConfig.h"

class IProvider
{
public:
	struct Feedback
	{
		unsigned int potential = 0;
		unsigned int sucess = 0;
		inline unsigned int getFailed() const { return potential - sucess; };
	};
	/// <summary>
	/// Provides a name shown in the UI
	/// </summary>
	/// <returns></returns>
	virtual QString GetProviderName() const = 0;

	/// <summary>
	/// Returns a list of all avaible templates
	/// </summary>
	virtual QList<TemplateConfig> GetTemplates(Feedback& feedback) const = 0;
	inline QList<TemplateConfig> GetTemplates() const { Feedback f; return GetTemplates(f); }

	/// <summary>
	/// Writes the template files to the specified path.
	/// </summary>
	virtual void CopyTemplateFiles(TemplateConfig cfg,QDir outputPath) const = 0;
};