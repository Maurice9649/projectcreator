#include "localProvider.h"
#include <QLoggingCategory>
#include <QJsonObject>
Q_LOGGING_CATEGORY(lcLocalProvider, "provider.local")

void LocalProvider::copyDirRecursive(QString inputPath, QString outputPath) const
{
	inputPath	= QDir::toNativeSeparators(inputPath);
	outputPath	= QDir::toNativeSeparators(outputPath);
	qCDebug(lcLocalProvider) << "Copying " << inputPath << " => " << outputPath;
	const auto sep = QDir::separator();
	QDir input { inputPath };
	if (!input.exists()) { return; };
	
	for (auto& subdir : input.entryList(QDir::Dirs | QDir::NoDotAndDotDot))
	{
		QString subOutputPath = outputPath + sep + subdir;
		input.mkpath(subOutputPath);
		copyDirRecursive(inputPath + sep + subdir, subOutputPath);
	}
	for (auto& fileName : input.entryList(QDir::Files))
	{
		auto inputFile = inputPath + sep + fileName;
		auto outputFile = outputPath + sep + fileName;
		qCDebug(lcLocalProvider) << "Copying " << inputFile << " => " << outputFile;
		QFile::copy(inputFile, outputFile);
	}
}

LocalProvider::LocalProvider(QDir path, QObject* parent)
	: QObject(parent) , Path(std::move(path))
{
	qCDebug(lcLocalProvider) << "New Provider for " << Path.absolutePath();
	Q_ASSERT(!Path.isEmpty() && Path.exists());
}

QList<TemplateConfig> LocalProvider::GetTemplates(Feedback& feedback) const
{
	QList<TemplateConfig> returnList {};
	QDir templateDir = Path;
	templateDir.cd("templates");
	if (!templateDir.exists() || templateDir.isEmpty() ) { return {}; }
	templateDir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);

	auto fileInfoList = templateDir.entryInfoList();
	feedback.potential = fileInfoList.size();
	for (auto& fileInfo : fileInfoList)
	{
		QDir folderPath = fileInfo.absoluteFilePath();
		qCDebug(lcLocalProvider) << "Processing Template" << folderPath.absolutePath();
		// Get Cfg
		if (!folderPath.exists("config.json"))
		{
			qCCritical(lcLocalProvider) << "Invalid Template (" << folderPath.absolutePath() << " ),missing a config.json file!";
			continue;
		}
		QFile cfgFile{ folderPath.filePath("config.json") };

		if (!cfgFile.open(QIODevice::ReadOnly | QIODevice::Text))
		{
			qCCritical(lcLocalProvider) << "Couldnt open file:" << cfgFile.fileName();
			continue;
		}
		// Parse Json
		QJsonParseError err;
		auto cfgJson = QJsonDocument::fromJson(cfgFile.readAll(), &err);
		if (cfgJson.isNull())
		{
			qCCritical(lcLocalProvider) << "Failed to parse Json (" << cfgFile.fileName() << ") reason:" << err.errorString();
			continue;
		}
		if (!cfgJson.isObject())
		{
			qCCritical(lcLocalProvider) << "Failed to parse Json (" << cfgFile.fileName() << ") reason:" << "Expected Object as root";
			continue;
		}

		try
		{
			auto cfgTemplate = TemplateConfig::fromJson(cfgJson.object());
			cfgTemplate.InitUniqueID(folderPath.absolutePath());
			returnList.append(std::move(cfgTemplate));
			feedback.sucess++;
		}
		catch (ParseException& e)
		{
			qCCritical(lcLocalProvider) << "Failed to parse Json (" << cfgFile.fileName() << ") reason:" << e.what();
			continue;
		}
	}

	return returnList;
}

void LocalProvider::CopyTemplateFiles(TemplateConfig cfg, QDir outputPath) const
{
	QDir templateFolder{ cfg.GetUniqueID() };
	Q_ASSERT(templateFolder.exists() && !templateFolder.isEmpty());

	if (!templateFolder.cd("files"))
	{
		// No Files to copy
		return;
	}
	copyDirRecursive(templateFolder.absolutePath(), outputPath.absolutePath());
}
