#include <gtest/gtest.h>

#include <QtCore>

#include "providers/localProvider.h"

#ifdef CREATOR_INCLUDE_EXAMPLES
TEST(LocalProviderTests, ParseExampleTemplate)
{
    QDir examplePath = QCoreApplication::applicationDirPath();
    examplePath.cd("examples");

    LocalProvider provider{ examplePath };
    IProvider::Feedback feedback {};
    auto templates = provider.GetTemplates(feedback);

    // Expect it to have found atleast 1 example.
    EXPECT_GT(templates.size(), 0);
    EXPECT_EQ(feedback.potential, feedback.sucess);
}

// Simple Test that just checks if copy doesn't fail
// currently does not check if the copy is being correctly
TEST(LocalProviderTests, CopyExampleTemplate)
{
    QDir examplePath = QCoreApplication::applicationDirPath();
    examplePath.cd("examples");

    LocalProvider provider{ examplePath };
    IProvider::Feedback feedback{};
    auto templates = provider.GetTemplates(feedback);

    // Expect it to have found atleast 1 example.
    ASSERT_GT(templates.size(), 0);
    
    // Make a TempFolder to copy to
    for (auto& templateCfg : templates)
    {
        QTemporaryDir tempDirMaker;
        ASSERT_TRUE(tempDirMaker.isValid());
        QDir tempDir{ tempDirMaker.path() };

        provider.CopyTemplateFiles(templateCfg, tempDir);
    }
}
#endif