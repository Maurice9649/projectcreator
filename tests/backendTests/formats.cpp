#include <gtest/gtest.h>
#include <variant>

#include <QtCore>

#include "formats/templateConfig.h"
#include "formats/option.h"
#include "formats/project.h"

using namespace std::literals;

class BaseOptionTests : public testing::TestWithParam<std::string>
{};

TEST_P(BaseOptionTests, BasicParse)
{
    const char* NAME = "Test Name";
    const char* HELP = "Test Help String";
    const char* FORMAT = "Test Format Hint";

    QJsonObject optJson
    {
        {"name", NAME},
        {"type", QString::fromStdString(GetParam()) },
        {"help", HELP},
        {"formatHint",FORMAT}
    };

    auto optVariant = Option::fromJson(optJson);
    auto option = optVariant.value<Option>();

    EXPECT_EQ(option.Name, NAME);
    EXPECT_EQ(option.Help, HELP);
    EXPECT_EQ(option.FormatHint, FORMAT);
}

INSTANTIATE_TEST_SUITE_P(
    BaseOptionTestsValues,
    BaseOptionTests,
    testing::Values(
        "String"s,
        "Int"s
    ),
    [](const ::testing::TestParamInfo<BaseOptionTests::ParamType>& info) {
        return info.param;
    }
);

TEST(ProjectTests, BasicParse)
{
    const char* STRING = "String";
    double NUMBER = 10.2;
    bool BOOL = true;
    ////
    Project testObj{};

    // Options
    testObj.OptionValues["String"] = STRING;
    testObj.OptionValues["Number"] = NUMBER;
    testObj.OptionValues["Bool"] = BOOL;

    //////
    auto testObjJson = testObj.toJson();

    auto optionsJson = testObjJson["options"].toObject();
    EXPECT_EQ(optionsJson["String"].toString(), STRING);
    EXPECT_EQ(optionsJson["Number"].toDouble(), NUMBER);
    EXPECT_EQ(optionsJson["Bool"].toBool(), BOOL);

}