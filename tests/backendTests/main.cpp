#include <gtest/gtest.h>
#include <variant>

#include <QtCore>

#include "formats/templateConfig.h"
#include "formats/option.h"
#include "formats/project.h"

int main(int argc, char** argv) {
    QCoreApplication a(argc, argv);
    ::testing::InitGoogleTest(&argc, argv);

    QTimer::singleShot(0, []()
        {
            QCoreApplication::exit(RUN_ALL_TESTS());
        }

    );

    return a.exec();
}
/*
int main(int argc, char* argv[])
{

    QJsonObject option
    {
        {"name","Project Name"},
        {"type","String"},
        {"help","Test"},
        {"formatHint","Test Format"}
    };

    QJsonObject templateConfig
    {
        {"name","Example Generic"},
        {"tags", QJsonArray { "generic", "example"}},
        {"type","Generic"},
        {"options", QJsonArray { option } },
        {"configureCommand","python ./configure.py"}
    };

    auto temp = TemplateConfig::fromJson(templateConfig);
    
    qDebug() << temp.Name;
    return a.exec();
}
*/